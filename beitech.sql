CREATE DATABASE BeitechTest;

USE BeitechTest;

#creating table product

CREATE TABLE product
(
product_id INT(10) PRIMARY KEY NOT NULL,
product_name VARCHAR (191) NOT NULL,
product_description VARCHAR (191) NOT NULL,
price Double(10,2) NOT NULL
);

#creating table customer

CREATE TABLE customer
(
customer_id INT(10) PRIMARY KEY NOT NULL,
customer_name VARCHAR (191) NOT NULL,
email VARCHAR (191) NOT NULL
);

#creating table customer_product

CREATE TABLE customer_product (
	customer_id  INT(10) NOT NULL,
	product_id  INT(10) NOT NULL,
	FOREIGN KEY (customer_id) REFERENCES customer (customer_id) ON DELETE RESTRICT ON UPDATE CASCADE,
	FOREIGN KEY (product_id) REFERENCES product (product_id) ON DELETE RESTRICT ON UPDATE CASCADE,
	PRIMARY KEY (customer_id, product_id)
);

#creating table orders
CREATE TABLE orders (
    order_id INT(10)  AUTO_INCREMENT NOT NULL,
    customer_id  INT(10)  NOT NULL,
    creation_date  DATE  NOT NULL,
    delivery_address  VARCHAR (191) NOT NULL,
    total Double(15,2) NOT NULL,
    PRIMARY KEY (order_id),
    FOREIGN KEY (customer_id) REFERENCES customer (customer_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

#creating table oder detail
CREATE TABLE order_detail (
    #order_detail_id INT(10)  NOT NULL,
    order_id INT(10)  NOT NULL,
    product_id  INT(10) NOT NULL,
    product_description VARCHAR (191) NOT NULL,
    price Double(10,2) NOT NULL,
    quantity INT(10) NOT NULL,
    #PRIMARY KEY (order_detail_id),
    PRIMARY KEY (order_id, product_id),
    FOREIGN KEY (order_id) REFERENCES orders (order_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (product_id) REFERENCES product (product_id) ON DELETE RESTRICT ON UPDATE CASCADE
);


#insert records in customers

INSERT INTO customer(customer_id, customer_name, email)
VALUES (1, "deiner", "drestrepoduran@hotmail.com");

INSERT INTO customer(customer_id, customer_name, email)
VALUES (2, "Manny Bharma", "manny_bharma@hotmail.com");

INSERT INTO customer(customer_id, customer_name, email)
VALUES (3, "Alan Briggs", "alan_briggs@hotmail.com");

INSERT INTO customer(customer_id, customer_name, email)
VALUES (4, "Mike Simm", "mike_simm@hotmail.com");


#insert records in products

INSERT INTO product(product_id, product_name, product_description, price)
VALUES (1, "Product A", "This is a Product A", 200);

INSERT INTO product(product_id, product_name, product_description, price)
VALUES (2, "Product B", "This is a Product B", 30);

INSERT INTO product(product_id, product_name, product_description, price)
VALUES (3, "Product C", "This is a Product C", 100);

INSERT INTO product(product_id, product_name, product_description, price)
VALUES (4, "Product D", "This is a Product D", 100);

#insert record INTo customer_prodcut

INSERT INTO customer_product(customer_id, product_id)
VALUES (1, 1);

INSERT INTO customer_product(customer_id, product_id)
VALUES (1, 3);

INSERT INTO customer_product(customer_id, product_id)
VALUES (2, 2);

INSERT INTO customer_product(customer_id, product_id)
VALUES (3, 1);

INSERT INTO customer_product(customer_id, product_id)
VALUES (3, 2);

INSERT INTO customer_product(customer_id, product_id)
VALUES (3, 4);

INSERT INTO customer_product(customer_id, product_id)
VALUES (4, 1);

#create orders
INSERT INTO orders(customer_id, creation_date, delivery_address, total)
VALUES (1, "2017-05-01", "calle 36 #4a40 los mayales", 500 );

INSERT INTO orders(customer_id, creation_date, delivery_address, total)
VALUES (3, "2017-06-01", "chapinero norte", 600 );

INSERT INTO orders( customer_id, creation_date, delivery_address, total)
VALUES (1, "2017-06-02", "centro comercial los mayales", 100 );

INSERT INTO orders(customer_id, creation_date, delivery_address, total)
VALUES (2, "2021-01-01", "puente aranda", 50 );


#insert order detail
INSERT INTO order_detail(order_id, product_id, product_description, price, quantity)
VALUES (1, 1,"This is a Product A" , 200, 2 );

INSERT INTO order_detail(order_id, product_id, product_description, price, quantity)
VALUES (1, 3,"This is a Product C" , 100, 1 );

INSERT INTO order_detail(order_id, product_id, product_description, price, quantity)
VALUES (2, 1,"This is a Product A" , 200, 2 );

INSERT INTO order_detail(order_id, product_id, product_description, price, quantity)
VALUES (2, 3,"This is a Product C" , 100, 1 );

INSERT INTO order_detail(order_id, product_id, product_description, price, quantity)
VALUES (3, 1,"This is a Product A" , 200, 2 );

INSERT INTO order_detail(order_id, product_id, product_description, price, quantity)
VALUES (4, 3,"This is a Product C" , 100, 1 );

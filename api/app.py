from flask_lambda import FlaskLambda
import json
from flask import request
import pymysql
from flask_cors import CORS, cross_origin

app = FlaskLambda(__name__)
CORS(app, resources={
    r"/*": {
        "origins": "*"
    }
})

endpoint = "beitech-test.ch2a96jey6a9.us-east-1.rds.amazonaws.com"
username = "admin"
password = "beitechsas"
database_name = "BeitechTest"

connection = pymysql.connect(host=endpoint,
                             user=username,
                             password=password,
                             database=database_name,
                             autocommit=True)


def query_sql(query: str, type: str):
    cursor = connection.cursor()
    cursor.execute(query)

    if type == "all":
        rows = cursor.fetchall()
    else:
        rows = cursor.fetchone()
    cursor.close()
    return rows


def get_product_by_id(id: str):
    query = "SELECT * FROM product WHERE product_id ='{id}'".format(id=str(id))
    product = query_sql(query, "one")

    detail_product = {
        "id": product[0],
        "name": product[1],
        "description": product[2],
        "price": product[3],
    }

    return detail_product


def get_products_by_customer(id: str):
    products_by_customer_list = []
    query_products_customer = "SELECT * FROM customer_product WHERE customer_id ='{id}'".format(id=str(id))
    products_customer = query_sql(query_products_customer, "all")

    for product in products_customer:
        products_by_customer_list.append(get_product_by_id(product[1]))

    return products_by_customer_list


def get_customer_by_id(id: str):
    query = "SELECT * FROM customer WHERE customer_id ='{id}'".format(id=str(id))
    customer = query_sql(query, "one")

    detail_customer = {
        "id": customer[0],
        "name": customer[1],
        "email": customer[2],
        "products": get_products_by_customer(customer[0])
    }

    return detail_customer


def get_detail_order(id: str):
    details_list = []
    query = "SELECT * FROM order_detail WHERE order_id ='{id}'".format(id=str(id))
    details = query_sql(query, "all")

    for detail in details:
        details_list.append({
            "order": detail[0],
            "product": get_product_by_id(detail[1]),
            "quantity": detail[4],
        })

    return details_list


@app.route('/customers', methods=['GET'])
def api_customers():
    customers_list = []

    customers = query_sql("SELECT * FROM customer", "all")

    for customer in customers:

        customers_list.append(
            {
                "id": customer[0],
                "name": customer[1],
                "email": customer[2],
                "products": get_products_by_customer(customer[0])
            }
        )

    return (
        json.dumps({"data": customers_list}),
        200,
        {'Content-Type': 'application/json'}
    )


@app.route('/customers/<id>', methods=['GET'])
def api_customer(id):
    customer = get_customer_by_id(id)
    return (
        json.dumps({"data": customer}),
        200,
        {'Content-Type': 'application/json'}
    )


@app.route('/products', methods=['GET'])
def api_products():
    products_list = []
    rows = query_sql("SELECT * FROM product", "all")

    for row in rows:
        products_list.append(
            {
                "id": row[0],
                "name": row[1],
                "description": row[2],
                "price": row[3],
            }
        )

    return (
        json.dumps({"data": products_list}),
        200,
        {'Content-Type': 'application/json'}
    )


@app.route('/orders', methods=['GET', 'POST'])
@cross_origin()
def api_orders():
    orders_list = []
    query_has_customer = False
    if request.method == 'GET':
        query = "SELECT * FROM orders"
        if request.args.get('customer') is not None:
            customer = request.args.get('customer')
            query = query + " WHERE customer_id ='{customer}'".format(customer=str(customer))
            query_has_customer = True
        if request.args.get('start_date') is not None:
            start_date = request.args.get('start_date')
            end_date = request.args.get('end_date')
            base_query = " creation_date BETWEEN '{start_date}' AND '{end_date}'".format(start_date=start_date,
                                                                                   end_date=end_date)
            if query_has_customer:
                query = query + " AND" + base_query
            else:
                query = query + " WHERE" + base_query

        orders = query_sql(query, "all")

        for order in orders:
            orders_list.append(
                {
                    "id": order[0],
                    "customer": get_customer_by_id(order[1]),
                    "creation_date": str(order[2]),
                    "delivery_address": order[3],
                    "price": order[4],
                    "detail": get_detail_order(order[0])
                }
            )

        return (
            json.dumps({"data": orders_list}),
            200,
            {'Content-Type': 'application/json'}
        )
    else:
        from datetime import datetime

        data = request.json['order']
        customer = data['customer']
        delivery_address = data['delivery_address']
        products = request.json['products']
        total = 0
        date = datetime.now().strftime("%Y-%m-%d")
        comand_insert = "INSERT INTO orders(customer_id, creation_date, delivery_address, total) VALUES" \
                        " ('{customer}', '{date}', '{address}', '{total}' )".format(customer=customer, date=date,
                                                                                  address=delivery_address, total=total)
        cursor = connection.cursor()
        cursor.execute(comand_insert)
        order_id = cursor.lastrowid

        for product in products:
            total += (product["price"] * product["quantity"])
            comand_insert_detail = "INSERT INTO order_detail(order_id, product_id, product_description, price, quantity) VALUES" \
                            " ('{order}', '{product}', '{description}', '{price}', '{quantity}' )".format(order=order_id,
                                                                                                      product=product["id"],
                                                                                                      description=product["description"],
                                                                                                      price=product["price"],
                                                                                                      quantity=product["quantity"],
                                                                                                      )
            cursor.execute(comand_insert_detail)
        command_update = "UPDATE orders SET total= '{total}' WHERE order_id='{id}'".format(total=total, id=str(order_id))
        cursor.execute(command_update)
        cursor.close()
        connection.commit()

        return (
            json.dumps({"OK": True}),
            200,
            {'Content-Type': 'application/json'}
        )


@app.route('/orders/<id>', methods=['GET'])
@cross_origin()
def api_order(id):
    query = "SELECT * FROM orders WHERE order_id ='{id}'".format(id=str(id))
    orders = query_sql(query, "one")
    order = {
        "id": orders[0],
        "customer": get_customer_by_id(orders[1]),
        "creation_date": str(orders[2]),
        "delivery_address": orders[3],
        "price": orders[4],
        "detail": get_detail_order(orders[0])
    }
    return (
        json.dumps({"data": order}),
        200,
        {'Content-Type': 'application/json'}
    )
